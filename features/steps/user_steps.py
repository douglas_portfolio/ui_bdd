from behave import *
import features.workflows.user_workflow as workflow
from features.pages.HomePage import HomePage

@given(u'I\'m on homepage')
def step_impl(context):
    home_page = HomePage()
    home_page.go()


@when(u'I create a new user')
def step_impl(context):
    context = workflow.create_user(context)


@then(u'New user should be created')
def step_impl(context):
    context = workflow.user_is_visible(context)
