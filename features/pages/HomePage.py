from features.pages.Page import Page
from selenium.webdriver.common.by import By


class HomePage(Page):
    CREATE_USER_BUTTON = (By.XPATH, "//a[contains(text(),'Create User')]")

    def __init__(self):
        Page.__init__(self)

    def go(self):
        self.driver.get('http://localhost:8080')

        return self

    def click_on_create_user(self):
        self.click(HomePage.CREATE_USER_BUTTON)

    def __is_load__(self):
        self.wait_visibility_of_element_located(HomePage.CREATE_USER_BUTTON)
