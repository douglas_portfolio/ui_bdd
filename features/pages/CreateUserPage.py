from features.pages.Page import Page
from selenium.webdriver.common.by import By


class CreateUserPage(Page):
    SUBMIT_BUTTON = (By.ID, 'submit')
    USERNAME = (By.ID, 'username')
    PASSWORD = (By.ID, 'password')
    EMAIL = (By.ID, 'email')
    BIRTH_DATE = (By.ID, 'birth_date')
    ADDRESS = (By.ID, 'address')

    def __init__(self):
        Page.__init__(self)

    def __is_load__(self):
        self.wait_visibility_of_element_located(CreateUserPage.SUBMIT_BUTTON)