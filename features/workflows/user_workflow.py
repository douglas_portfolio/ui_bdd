from features.pages.HomePage import HomePage
from features.pages.CreateUserPage import CreateUserPage
from features.pages.SeeUsersPage import SeeUsersPage
import random

home_page = HomePage()
create_page = CreateUserPage()
see_page = SeeUsersPage()

def create_user(context):
    home_page.click_on_create_user()

    context.name = 'mauro'

    create_page.__is_load__()
    create_page.send_keys(create_page.USERNAME, context.name)
    create_page.send_keys(create_page.PASSWORD, 'hahauahauh')
    create_page.send_keys(create_page.EMAIL, 'x{}@test.com'.format(random.randint(0, 500)))
    create_page.send_keys(create_page.BIRTH_DATE, '18/11/21')
    create_page.send_keys(create_page.ADDRESS, 'uahauhauhauah')
    create_page.click(create_page.SUBMIT_BUTTON)

    return context

def user_is_visible(context):
    see_page.__is_load__()
    see_page.is_user_visible(context.name)

    return context