from selenium import webdriver


def before_all(context):
    driver_path = 'features/drivers/{}'
    driver = webdriver.PhantomJS(executable_path=driver_path.format('phantomjs'))
    context.browser = driver


def after_all(context):
    context.browser.quit()